#include "MainWindow.h"

#include <Windows.h>
#include <tchar.h>

const int wndMinHeight = 450;
const int wndMinWidth = 800;

void MainWindow::OnPaint(HWND hWnd)
{
	sprite->Paint(hWnd);
	InvalidateRect(hWnd, NULL, FALSE);
}

void MainWindow::OnGetMinMaxInfo(UINT param)
{
	LPMINMAXINFO lpMinMaxInfo = (LPMINMAXINFO)param;
	lpMinMaxInfo->ptMinTrackSize.x = wndMinWidth;
	lpMinMaxInfo->ptMinTrackSize.y = wndMinHeight;
	InvalidateRect(hWnd, NULL, FALSE);
}

void MainWindow::OnKeyDown(UINT param)
{
	switch (param)
	{
	case VK_UP:
	{
		sprite->MoveUp();
		break;
	}
	case VK_RIGHT:
	{
		sprite->MoveRight();
		break;
	}
	case VK_DOWN:
	{
		sprite->MoveDown();
		break;
	}
	case VK_LEFT:
	{
		sprite->MoveLeft();
		break;
	}
	case VK_ADD: {
		sprite->Upscale();
		break;
	}
	case VK_SUBTRACT: {
		sprite->Downscale();
		break;
	}
	default:
		break;
	}
	InvalidateRect(hWnd, NULL, FALSE);
}

void MainWindow::OnMouseWheel(UINT param)
{
	WPARAM mouseKeyStates = param;
	short delta = GET_WHEEL_DELTA_WPARAM(param);
	bool shiftPressed = mouseKeyStates & MK_SHIFT;
	if (shiftPressed) {
		delta < 0 ? sprite->MoveLeft() : sprite->MoveRight();
	}
	else {
		delta < 0 ? sprite->MoveDown() : sprite->MoveUp();
	}

	InvalidateRect(hWnd, NULL, false);
}

LRESULT CALLBACK MainWindow::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{

	case WM_KEYDOWN: {
		MainWindow::OnKeyDown(wParam);
		break;
	}
	case WM_MOUSEWHEEL:
	{
		MainWindow::OnMouseWheel(wParam);
		break;
	}
	
	case WM_GETMINMAXINFO: {
		MainWindow::OnGetMinMaxInfo(lParam);
		break;
	}
	case WM_PAINT:
	{
		MainWindow::OnPaint(hWnd);

		break;
	}
	case WM_SIZE: {
		InvalidateRect(hWnd, NULL, false);
		break;
	}
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}
