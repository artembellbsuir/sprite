#include "Sprite.h"

Sprite::Sprite(HWND parenthWnd, int xCoord, int yCoord)
{
	this->parenthWnd = parenthWnd;

	this->xCoord = xCoord;
	this->yCoord = yCoord;

	this->xVelocity = 10;
	this->yVelocity = 10;

	hSpriteBmp = (HBITMAP)LoadImage(NULL, L"flag.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	BITMAP spriteBmpInfo;
	GetObject(hSpriteBmp, sizeof(spriteBmpInfo), &spriteBmpInfo);

	this->width = spriteBmpInfo.bmWidth;
	this->height = spriteBmpInfo.bmHeight;
}


void Sprite::Paint(HWND hWnd)
{
	PAINTSTRUCT     ps;
	BITMAP          spriteBmpInfo;

	HDC hdcWnd = BeginPaint(hWnd, &ps);

	RECT rect;
	GetClientRect(hWnd, &rect);
	/*SetRect(&rect);*/
	FillRect(hdcWnd, &rect, (HBRUSH)(COLOR_WINDOW + 1));

	// create memory DC
	HDC hdcMem = CreateCompatibleDC(hdcWnd);





	// select created bmp for this DC (old bitmap stores 1x1 bmp)
	HGDIOBJ oldBmp = SelectObject(hdcMem, hSpriteBmp);
	// retreives info about our hSpriteBmp and stores it in bitmap
	GetObject(hSpriteBmp, sizeof(spriteBmpInfo), &spriteBmpInfo);
	// copy image from memory DC to window DC

	StretchBlt(hdcWnd, xCoord, yCoord, this->width, this->height,
		hdcMem, 0, 0, spriteBmpInfo.bmWidth, spriteBmpInfo.bmHeight,
		SRCCOPY);

	/*BitBlt(hdcWnd, xCoord, yCoord, this->width, this->height,
		hdcMem, 0, 0,
		SRCCOPY);*/

	SelectObject(hdcMem, oldBmp);
	// delete DC with its bitmap
	DeleteDC(hdcMem);

	EndPaint(hWnd, &ps);
}

void Sprite::MoveUp()
{
	if (this->yCoord - this->yVelocity > 0) {
		this->yCoord -= yVelocity;
	}
	else {
		this->yCoord += 2 * yVelocity;
	}
}

void Sprite::MoveRight()
{
	if (this->xCoord + this->xVelocity < 500 - this->width) {
		this->xCoord += xVelocity;
	}
	else {
		this->xCoord -= 2 * xVelocity;
	}
}

void Sprite::MoveDown()
{
	if (this->yCoord + this->yVelocity < 500 - this->height) {
		this->yCoord += yVelocity;
	}
	else {
		this->yCoord -= 2 * yVelocity;
	}
}

void Sprite::MoveLeft()
{
	if (this->xCoord - this->xVelocity > 0) {
		this->xCoord -= xVelocity;
	}
	else {
		this->xCoord += 2 * xVelocity;
	}
}

void Sprite::Scale(double scale = 1.05) {
	int newHeight = (double)this->height * scale;
	int newWidth = (double)this->width * scale;

	double xChange = (this->height - newHeight) / 2;
	double yChange = (this->width - newWidth) / 2;

	this->height = newHeight;
	this->width = newWidth;

	this->xCoord += xChange;
	this->yCoord += yChange;
}

void Sprite::Upscale() {
	const double scale = scaleDefault;
	Scale(scale);
}

void Sprite::Downscale() {
	const double scale = 1 / scaleDefault;
	Scale(scale);
}
