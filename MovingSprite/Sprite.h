#pragma once

#pragma once

#include "windows.h"

class Sprite
{
private:
	HBITMAP hSpriteBmp;
	HWND parenthWnd;


	double scaleDefault = 1.05;

	void Scale(double scale);


public:
	Sprite(HWND, int, int);


	int xCoord, yCoord;
	int xVelocity, yVelocity;
	int width, height;

	void Paint(HWND);

	void MoveUp();
	void MoveRight();
	void MoveDown();
	void MoveLeft();

	void Upscale();
	void Downscale();
};