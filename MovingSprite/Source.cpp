#include <tchar.h>
#include <Windows.h>

#include "MainWindow.h"
#include "Application.h"
#include "Sprite.h"

HINSTANCE MainWindow::hInstance = NULL;
HWND MainWindow::hWnd = NULL;
Sprite* MainWindow::sprite = NULL;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{
	
	MainWindow::hInstance = hInstance;
	Application::Main(nCmdShow);
}