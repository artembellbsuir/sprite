#ifndef __MainWindow__
#define __MainWindow__

#include <Windows.h>
#include "Sprite.h"

class MainWindow {
public:
	static HWND hWnd;
	static HINSTANCE hInstance;
	static Sprite* sprite;

	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
	static void OnPaint(HWND hWnd);
	static void OnGetMinMaxInfo(UINT param);
	static void OnKeyDown(UINT param);
	static void OnMouseWheel(UINT param);
};

#endif