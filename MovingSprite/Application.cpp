#include <Windows.h>

#include "MainWindow.h"
#include "Application.h"
#include "Sprite.h"

BOOL Application::Main(int nCmdShow)
{
	// Register the window class.
	const wchar_t CLASS_NAME[] = L"Sample Window Class";

	WNDCLASS wc = { };

	wc.lpfnWndProc = MainWindow::WindowProc;
	wc.hInstance = MainWindow::hInstance;
	wc.lpszClassName = CLASS_NAME;

	RegisterClass(&wc);

	// Create the window.

	MainWindow::hWnd = CreateWindowEx(
		0,                              // Optional window styles.
		CLASS_NAME,                     // Window class
		L"Learn to Program Windows",    // Window text
		WS_OVERLAPPEDWINDOW,            // Window style

		// Size and position
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

		NULL,       // Parent window    
		NULL,       // Menu
		MainWindow::hInstance,  // Instance handle
		NULL        // Additional application data
	);

	MainWindow::sprite = new Sprite(MainWindow::hWnd, 200, 200);

	if (MainWindow::hWnd == NULL)
	{
		return 0;
	}

	ShowWindow(MainWindow::hWnd, nCmdShow);

	// Run the message loop.

	MSG msg = { };
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return 0;
}
